package guava_implementations;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Joiner;

/**
 * Joiners are immutable, threadsafe, and usable as a static final constant
 * Can do strings and also string representations of objects
 */
public class Joiners {



    private static String joinerSkipNulls(Collection<? extends CharSequence> c, String del) {
        return Joiner.on(del).skipNulls().join(c);
    }

    private static String joinerToStringObjects(Collection<?> c, String del) {
        return Joiner.on(del).join(c);

    }




    public static void main(String[] args) {

        List<String> stringsNoNulls = List.of("Thomas", "is", "awesome");
        String del = ":";
        System.out.println(joinerSkipNulls(stringsNoNulls, del));

        List<String> stringWithNulls = Arrays.asList("Thomas", null, null, "Cools");
        del = "; ";
        System.out.println(joinerSkipNulls(stringWithNulls, del));


        System.out.println(joinerToStringObjects(Arrays.asList(1, 100, 250), del));
    }

}
