package effective_java;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class Optionals {

    /**
     * Here you throw exception for empty collection
     * @param c
     * @param <E>
     * @return
     * @throws IllegalArgumentException
     */
    private static <E extends Comparable<E>> E max(Collection<E> c) {
        if (c.isEmpty()) {
            throw new IllegalArgumentException("Empty collection");
        }

        E result = null;
        for (E e : c) {
            if (result == null || e.compareTo(result) > 0) {
                result = Objects.requireNonNull(e);
            }
        }

        return result;
    }

    /**
     * Here you just return Optional to avoid throwing exception
     * @param c
     * @param <E>
     * @return
     */
    private static <E extends Comparable<E>> Optional<E> maxUsingOptional(Collection<E> c) {
        if (c.isEmpty()) {
            return Optional.empty();
        }

        E result = null;

        for (E e : c) {
            if (result == null || e.compareTo(result) > 0) {
                result = Objects.requireNonNull(e);
            }
        }

        return Optional.of(result);
    }

    /**
     * Here streams automatically returns an Optional
     * @param c
     * @param <E>
     * @return
     */
    private static <E extends Comparable<E>> Optional<E> maxStreams(Collection<E> c) {
        return c.stream().max(Comparator.naturalOrder());
    }

    public static void main(String[] args) {

        List<Integer> emptyList = Collections.emptyList();

        List<Integer> nonEmptyList = Arrays.asList(Integer.valueOf(1), Integer.valueOf(50), Integer.valueOf(-20));

        // Need to catch the exception
        try {
            max(emptyList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Prints Optional.empty
        System.out.println(maxUsingOptional(emptyList).toString());
        System.out.println(maxStreams(emptyList).toString());

        // First prints 50, second two print Optional[50]
        System.out.println(max(nonEmptyList));
        System.out.println(maxUsingOptional(nonEmptyList));
        System.out.println(maxStreams(nonEmptyList));
    }
}
