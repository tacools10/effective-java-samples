package effective_java;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicSerialNum {

    private static final AtomicLong nextSerialNum = new AtomicLong();

    /**
     * Atomic package provides lock-free, thread-safe programming on single
     * variables. Have both communication effects of synchronicty (which
     * volatile also provides) but also provide atomicity.
     * @return
     */
    public static long generateSerialNumber() {
        return nextSerialNum.getAndIncrement();
    }
}
