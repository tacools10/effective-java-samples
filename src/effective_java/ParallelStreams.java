package effective_java;

import java.math.BigInteger;
import java.util.stream.LongStream;

public class ParallelStreams {


    /**
     * In this case on my machine I get a speed-up from 210s
     * to 46s to find all primes  between 2 and 10^8 when
     * using parallel streams.
     * @param n
     * @return
     */
    public static long countPrimesParallel(long n) {
        return LongStream.rangeClosed(2, n)
                .parallel()
                .mapToObj(BigInteger::valueOf)
                .filter(i -> i.isProbablePrime(50))
                .count();
    }

    public static long countPrimesSequential(long n) {
        return LongStream.rangeClosed(2, n)
                .mapToObj(BigInteger::valueOf)
                .filter(i -> i.isProbablePrime(50))
                .count();
    }

    public static void main(String[] args){

        long startTime = System.nanoTime();

        long count  = countPrimesParallel(100_000_000L);

        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        System.out.println("Parallelized stream took: " + totalTime/(1_000_000_000L) + " to complete. Found: " + count + " primes");

        startTime = System.nanoTime();

        count  = countPrimesSequential(100_000_000L);

        endTime = System.nanoTime();
        totalTime = endTime - startTime;
        System.out.println("Single threaded stream took: " + totalTime/(1_000_000_000L) + " to complete. Found: " + count + " primes");


    }
}
