package effective_java;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SubLists {

    /**
     * Example below explanation
     *
     Prefixes
     (1)
     (1,2)
     (1,2,3)

     Suffixes of Prefixes
     (1)
     (1,2), (2),
     (1, 2, 3), (1,2), (3)

     Combining them
     (1), (1,2), (1,2,3), (1,2), (2), (3)
     * @param list
     * @param <E>
     * @return
     */
    public static <E> Stream<List<E>> of(List<E> list) {
        return Stream.concat(Stream.of(Collections.emptyList()),
                             prefixes(list).flatMap(SubLists::suffixes));
    }

    private static <E> Stream<List<E>> prefixes(List<E> list) {
        return IntStream.rangeClosed(1, list.size())
                .mapToObj(end -> list.subList(0, end));
    }

    private static <E> Stream<List<E>> suffixes(List<E> list) {
        return IntStream.range(0, list.size())
                .mapToObj(start -> list.subList(start, list.size()));
    }

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(Integer.valueOf(1));
        list.add(Integer.valueOf(2));
        list.add(Integer.valueOf(3));

        Stream<List<Integer>> listStream = SubLists.of(list);

        System.out.println(Arrays.toString(listStream.toArray()));
    }

}


