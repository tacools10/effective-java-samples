package effective_java;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OverloadedMethodSelection {

    private static class CollectionClassifier {

        static String classify(Set<?> set) {
            return "Set";
        }

        static String classify(List<?> list) {
            return "List";
        }

        static String classify(Collection<?> collection) {
            return "Unknown collection";
        }

        static String correctedClassify(Collection<?> collection) {
            return collection instanceof Set ? "Set" :
                    collection instanceof List ? "List" : "Unknown Collection";
        }
    }

    /**
     * Basic inner class for method overriding demo
     */
    private static class Wine {

        static Wine getInstance() {
            return new Wine();
        }

        String name() {
            return "wine";
        }
    }

    private static class SparklingWine extends Wine {

        static SparklingWine getInstance() {
            return new SparklingWine();
        }

        @Override String name() {
            return "sparkling wine";
        }
    }

    private static class Champagne extends Wine {

        static Champagne getInstance() {
            return new Champagne();
        }

        @Override
        String name() {
            return "champagne";
        }
    }

    /**
     * Static method overloading (compile-time)
     * Here you get "Unknown collection" returned each time.
     * Method overloading choice is made at compile-time, so it's static.
     * In each case the type is Collection<?> so that's what is printed.
     * Corrected classify method is based on instanceof.
     */
    public static void main(String[] args) {
        Collection<?>[] collections  = {
                new HashSet<String>(),
                new ArrayList<BigInteger>(),
                new HashMap<String, String>().values()
        };

        for (Collection<?> c : collections) {
            System.out.println(CollectionClassifier.classify(c));
            System.out.println(CollectionClassifier.correctedClassify(c));
        }

        List<Wine> wineList = Arrays.asList(OverloadedMethodSelection.Wine.getInstance(), OverloadedMethodSelection.Champagne.getInstance(),
                                            OverloadedMethodSelection.SparklingWine.getInstance());

        /**
         * Prints correct name in each case, method overloaded at runtime based on type of object calling the name() method
         */
        for (Wine wine : wineList) {
            System.out.println(wine.name());
        }
    }

}
