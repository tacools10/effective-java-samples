package effective_java;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Curl {


     public static void curl(String url)throws IOException {
         try (InputStream in = new URL(url).openStream()) {
             in.transferTo(System.out);
         }
     }

     public static void main(String[] args) throws IOException {
         curl("http://www.google.com");
     }

}
