package effective_java;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DefensiveCopies {

    private List<Period> periods;
    private List<ZonedDateTimePeriod> zonedPeriods;

    private DefensiveCopies(List<Period> periods, List<ZonedDateTimePeriod> zonedPeriods) {
        this.periods = periods;
        this.zonedPeriods = zonedPeriods;
    }

    private List<Period> getPeriods() {
        return this.periods;
    }

    private List<ZonedDateTimePeriod> getZonedPeriods() {
        return this.zonedPeriods;
    }


    private final static class Period {

        private final Date start;
        private final Date end;


        /**
        *
        * @param start the beginning of the period
        * @param end the end of the period
        * @throws IllegalArgumentException if start is after end
        * @throws NullPointerException if start or end is null
        */
        Period(Date start, Date end) {
            if (start.compareTo(end) < 0) {
                throw new IllegalArgumentException(start + " after " + end);
            }
            this.start = start;
            this.end = end;
        }

        Period(Date start, Date end, boolean defensiveCopy) {
            this.start = new Date(start.getTime());
            this.end   = new Date(end.getTime());

            if (this.start.compareTo(this.end) > 0)  {
                throw new IllegalArgumentException(start + " after " + end);
            }
        }

        Date start() {
            return start;
        }

        Date end() {
            return end;
        }

        void modifyPeriodEnd(int year) {
            this.end.setYear(year);
        }
    }

    private final static class ZonedDateTimePeriod {

        private final ZonedDateTime start;
        private final ZonedDateTime end;

        ZonedDateTimePeriod(ZonedDateTime start, ZonedDateTime end) {
            this.start = ZonedDateTime.ofInstant(start.toInstant(), start.getZone());
            this.end   = ZonedDateTime.ofInstant(end.toInstant(), end.getZone());

            if (start.compareTo(end) > 0) {
                throw new IllegalArgumentException(start + " after " + end);
            }
        }

        ZonedDateTime start() {
            return start;
        }

        ZonedDateTime end() {
            return end;
        }

        /**
         *
         * @param yearsToAdd number of years to add to current Period end
         * @return new ZonedDateTime object with modified end date
         */
        ZonedDateTime modifyPeriodEnd(int yearsToAdd) {
            // Nothing to modify here as ZonedDateTime is immutable
            return ZonedDateTime.of(end.toLocalDateTime().with(temporal -> temporal.plus(yearsToAdd, ChronoUnit.YEARS)), end.getZone());
        }

    }

    public static void main(String[] args) {
        Date start = new Date();
        Date end = new Date();
        ZonedDateTime newStart = ZonedDateTime.now();
        ZonedDateTime newEnd = ZonedDateTime.now();
        Period p1 = new Period(start, end);
        Period p2 = new Period(start, end, true);
        ZonedDateTimePeriod p3 = new ZonedDateTimePeriod(newStart, newEnd);

        DefensiveCopies defensiveCopies = new DefensiveCopies(Arrays.asList(p1, p2), Arrays.asList(p3));
        defensiveCopies.periods.get(0).modifyPeriodEnd(2025);
        defensiveCopies.periods.get(1).modifyPeriodEnd(2024);


        System.out.println(defensiveCopies.getPeriods().get(0).end.getYear());
        System.out.println(defensiveCopies.getPeriods().get(1).end.getYear());
        System.out.println("Original object is: " + defensiveCopies.getZonedPeriods().get(0).toString());
        System.out.println(defensiveCopies.getZonedPeriods().get(0).end.getYear());

        // You need to create a new ZonedDateTimePeriod instance as there is no way to set the new end period due to immutability and final fields
        defensiveCopies.getZonedPeriods().set(0, new ZonedDateTimePeriod(newStart, defensiveCopies.getZonedPeriods().get(0).modifyPeriodEnd(10)));

        System.out.println("Modified object is: " + defensiveCopies.getZonedPeriods().get(0).toString());
        System.out.println("New year is: " + defensiveCopies.getZonedPeriods().get(0).end.getYear());

    }

}