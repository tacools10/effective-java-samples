package effective_java;

import java.math.BigInteger;
import java.util.Objects;

public class MethodParameterValidation {

    private BigInteger val;

    private MethodParameterValidation(String val) {
        this.val = new BigInteger(val);
    }

    /**
     * Returns a BigInteger whose value is (this mod m). This method  * differs from the remainder method in that it always returns a  
     * non-negative BigInteger.  
     * @param m the modulus, which must be positive
     * @return this mod m
     * @throws ArithmeticException if m is less than or equal to 0
     *
     * The actual BigInteger implementation uses a remainder method, here I just use the mod method from the BigInteger class
     * as the purpose is really to show how methods should catch illegal argument exceptions. 
     */
    private BigInteger mod(BigInteger m) {
        if (m.signum() <= 0) throw new ArithmeticException("Modulus <= 0: " + m);
            return this.val.mod(m);
    }

    /**
     * Simple method to print String to stdout
     * @param str
     * @throws NullPointerException when str is null
     */
    private void printNonNullString(String str) {
        System.out.println(Objects.requireNonNull(str, "String argument cannot be null"));
    }

    public static void main(String[] args) {

        MethodParameterValidation MethodParameterValidation = new MethodParameterValidation("1259");

        BigInteger remainder = MethodParameterValidation.mod(new BigInteger("16"));
        System.out.println(remainder.toString());

        // Here we throw an Arithmetic Exception as modulus cannot be negative
        try {
            remainder = MethodParameterValidation.mod(new BigInteger("-1"));
            System.out.println(remainder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Below we throw an NPE when String parameter is null
        try {
            MethodParameterValidation.printNonNullString("test");
            MethodParameterValidation.printNonNullString(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
