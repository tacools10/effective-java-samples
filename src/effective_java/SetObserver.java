package effective_java;

@FunctionalInterface  public interface SetObserver<E> {
    // Invoked when al element is added to the observable set
    void added(ObservableSet<E> set, E element);
}
