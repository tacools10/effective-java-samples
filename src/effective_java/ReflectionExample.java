package effective_java;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ReflectionExample {

    public static void getSetImplementationAndPrintContents(List<String> args)  {
        Class<? extends Set<String>> c1 = null;
        try {
            c1 = (Class<? extends Set<String>>) Class.forName(args.get(0));
        } catch(ClassNotFoundException e) {
            fatalError("Class not found");
        }

        Constructor<? extends Set<String>> cons = null;

        try {
            cons = c1.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            fatalError("No parameterless constructor");
        }

        // Instantiate the set
        Set<String> s = null;
        try {
            s = cons.newInstance();
        } catch (IllegalAccessException e) {
            fatalError("Constructor not accessible");
        } catch (InstantiationException e) {
            fatalError("Class not instantiable");
        } catch (InvocationTargetException e) {
            fatalError("Constructor threw " + e.getCause() );
        } catch (ClassCastException e) {
            fatalError("Class doesn't implement set");
        }

        s.addAll(args.subList(1, args.size()));
        System.out.println(s.toString());

    }

    private static void fatalError(String msg) {
        System.err.println(msg);
        System.exit(1);
    }

    private enum SetTypes {

        HASH_SET("java.util.HashSet"),
        TREE_SET("java.util.TreeSet");

        private final String className;

        SetTypes(String s) {
            this.className = s;
        }

        public String getClassName() {
            return this.className;
        }
    }

    public static void main(String[] args) {

        List<String> hashSet = new ArrayList<>();
        SetTypes setTypes = SetTypes.HASH_SET;
        hashSet.add(setTypes.getClassName());
        System.out.println(hashSet.toString());
        hashSet.addAll(Arrays.asList("Hello", "I", "win", "i", "all", "be"));
        getSetImplementationAndPrintContents(hashSet);

        hashSet.set(0, SetTypes.TREE_SET.getClassName());
        getSetImplementationAndPrintContents(hashSet);
    }

}
