package effective_java;

public class DetailedIndexOutOfBoundsException extends Exception {

    private int lowerBound;
    private int upperBound;
    private int index;

    /**
     * Constructs an IndexOutOfBoundsException.
     *
     * @param lowerBound the lowest legal index value
     * @param upperBound the highest legal index value plus one
     * @param index      the actual index value
     */
    public DetailedIndexOutOfBoundsException(int lowerBound, int upperBound, int index) {

        super(String.format("Lower bound: %d, Upper bound: %d, Index: %d",
                            lowerBound, upperBound, index));

        // save failure information for programmatic access
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.index      = index;
    }

}
